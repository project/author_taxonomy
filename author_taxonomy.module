<?php

/**
 * @file
 * Identifies authors using terms from a vocabulary.
 *
 * This module supports displaying multiple authors per node with
 * fully themable output.
 */


/**
 * Implementation of hook_menu().
 */
function author_taxonomy_menu() {
  $items = array();

  $items['admin/settings/author_taxonomy'] = array(
    'title' => 'Author taxonomy',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('author_taxonomy_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'description' => 'Change settings for Author taxonomy.',
  );

  return $items;
}


/**
 * Provides the settings options for the module.
 *
 * @return array
 *  The settings form.
 */
function author_taxonomy_admin_settings() {
  $form = array();

  // Get the list of vocabularies
  $vocabs = taxonomy_get_vocabularies();
  if (empty($vocabs)) {
    drupal_set_message(t('You have not yet created any vocabularies. Please <a href="@taxonomy_url">create a vocabulary</a> and return to this page.', array('@taxonomy_url' => url('admin/content/taxonomy/add/vocabulary'))), 'error');
  }
  foreach ($vocabs as $vid => $obj) {
    $vocabs[$vid] = $obj->name;
  }

  // Make list of available date formats
  $date_formats = array(
    'small' => 'Short',
    'medium' => 'Medium',
    'large' => 'Long',
    'custom' => 'Custom (below)',
  );

  // Drop-down box to choose author vocabulary
  $form['author_taxonomy_vocab'] = array(
    '#type' => 'select',
    '#title' => t('Author taxonomy'),
    '#options' => $vocabs,
    '#default_value' => variable_get('author_taxonomy_vocab', 1),
    '#description' => t('The taxonomy to use for content authors.'),
  );

  $form['author_taxonomy_prepend_node'] = array(
    '#type' => 'checkbox',
    '#title' => t('Automatically add output to the beginning of the node'),
    '#default_value' => variable_get('author_taxonomy_prepend_node', TRUE),
    '#description' => t('Check this box if you want to automatically display the author and timestamp info above each node\'s body text. If you uncheck this box, you must manually add the following line to your theme\'s node.tpl.php file wherever you want this information to appear: <strong>!code</strong>.', array('!code' => '&lt;?php print author_taxonomy_output($node); ?&gt;')),
  );

  $form['author_taxonomy_link_authors'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display authors\' names as links to their taxonomy pages'),
    '#default_value' => variable_get('author_taxonomy_link_authors', TRUE),
    '#description' => t('Checking this box will display each author\'s name as a link to his/her taxonomy page, effectively providing the user with a list of all nodes credited to that author.'),
  );

  $form['timestamp'] = array(
    '#type' => 'fieldset',
    '#title' => t('Timestamp display options'),
    '#description' => t('Customize or disable the display of the timestamp (node creation time and date).'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

    $form['timestamp']['author_taxonomy_timestamp_display'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display timestamp'),
      '#default_value' => variable_get('author_taxonomy_timestamp_display', TRUE),
      '#description' => t('Checking this box will display the timestamp. If you turn it off, you can ignore the other timestamp-related settings below.'),
    );

    $form['timestamp']['author_taxonomy_timestamp_type'] = array(
      '#type' => 'select',
      '#title' => t('Date output type'),
      '#options' => $date_formats,
      '#default_value' => variable_get('author_taxonomy_timestamp_type', array('medium')),
      '#description' => t('These options are defined on the !date_and_time settings page. If you choose "Custom," remember to enter the custom string below.', array('!date_and_time' => l(t('date and time'), 'admin/settings/date-time'))),
    );

    $form['timestamp']['author_taxonomy_timestamp_format'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom date format'),
      '#description' => t('Enter a custom date string using the PHP !date_link notation. Note: The "Custom" format must be selected above.', array('!date_link' => l('date() function', 'http://www.php.net/date'))),
      '#default_value' => variable_get('author_taxonomy_timestamp_format', 'j F Y'),
      '#maxlength' => 100,
      '#size' => 20,
    );

  return system_settings_form($form);
}


/**
 * Implementation of hook_nodeapi().
 */
function author_taxonomy_nodeapi(&$node, $op, $teaser, $page) {
  switch ($op) {
    case 'view' :
      $display = variable_get('author_taxonomy_prepend_node', TRUE);
      if ($display && ($page || $teaser)) {
        $node->content['body']['#value'] = author_taxonomy_output($node) ."\n". $node->content['body']['#value'];
      }
      break;
  }
}


/**
 * Creates array of authors' names and prepares output for the theme functions
 */
function author_taxonomy_output($node) {
  $author_tids = taxonomy_node_get_terms_by_vocabulary($node, variable_get('author_taxonomy_vocab', 1));
  $author_names = array();
  $author_links = variable_get('author_taxonomy_link_authors', TRUE);

  if (!empty($author_tids)) {
    foreach ($author_tids as $tid => $term) {
      if ($author_links) {
        $author_names[] = l($term->name, taxonomy_term_path($term), array('title' => t('See all stories by @author', array('@author' => $term->name))));
      }
      else {
        $author_names[] = $term->name;
      }
    }
  }

  $show_timestamp = variable_get('author_taxonomy_timestamp_display', TRUE);

  return theme('author_taxonomy_output', $node, $author_names, $show_timestamp);
}


/**
 * Implementation of hook_theme().
 */
function author_taxonomy_theme() {
  return array(
    'author_taxonomy_output' => array(
      'arguments' => array(
        'node' => NULL,
        'author_names' => NULL,
        'show_timestamp' => TRUE,
      ),
    ),
    'author_taxonomy_authors' => array(
      'arguments' => array(
        'author_names' => NULL,
      ),
    ),
    'author_taxonomy_timestamp' => array(
      'arguments' => array(
        'timestamp' => NULL,
      ),
    ),
  );
}


/**
 * Returns div containing authors and timestamp (themeable)
 */
function theme_author_taxonomy_output($node, $author_names, $show_timestamp = TRUE) {
  $output = '';

  if ($show_timestamp) {
    // Print timestamp
    $output .= '<span class="timestamp">'. theme('author_taxonomy_timestamp', $node->created) .'</span>';
  }

  if ($show_timestamp && !empty($author_names)) {
    // Print separator
    $output .= ' | ';
  }

  if (!empty($author_names)) {
    // Print authors
    $output .= '<span class="authors">'. theme('author_taxonomy_authors', $author_names) .'</span>';
  }

  // Wrap everything in div.submitted and return it
  return '<div class="submitted">'. $output .'</div>';
}


/**
 * Returns serialized list of authors' names (themeable)
 */
function theme_author_taxonomy_authors($author_names) {
  $output = '';
  $author_count = count($author_names);

  switch ($author_count) {
    case 0 : // No authors (theme_author_taxonomy_output() should have caught this)
      break;

    case 1 : // One author (output: "Name1")
      $output .= implode('', $author_names);
      break;

    case 2 : // Two authors (output: "Name1 and Name2")
      $output .= implode(' and ', $author_names);
      break;

    default : // More than two authors (output: "Name1, Name2, and Name3")
      $i = 1;
      foreach ($author_names as $author_name) {
        // If this is the last author
        if ($i == $author_count) {
          $output .= 'and '. $author_name;
        }
        else {
          $output .= $author_name .', ';
        }
        $i++;
      }
      break;
  }

  return $output;
}


/**
 * Returns the timestamp in the format chosen on the admin settings page (themeable)
 */
function theme_author_taxonomy_timestamp($timestamp) {
  $type = variable_get('author_taxonomy_timestamp_type', array('medium'));
  $format = variable_get('author_taxonomy_timestamp_format', 'j F Y');

  return format_date($timestamp, $type, $format);
}
